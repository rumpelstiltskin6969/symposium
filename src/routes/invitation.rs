use crate::db;
use serde::{Serialize, Deserialize};

use crate::models::invitation::InvitationUrl;
use rocket_contrib::json::Json;
use rocket_contrib::databases::rusqlite::Connection;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct InviteData {
    pub tree_id: i64,
    pub capacity: i32,
    pub expiry_days: i32,
}

#[post("/invitation/create", format="json", data="<invite>")]
pub fn create_route(conn: db::Conn, invite: Json<InviteData>) -> Json<InvitationUrl> {
    create(&conn, &invite)
}

pub fn create(
    conn: &Connection,
    invitation: &Json<InviteData>,
) -> Json<InvitationUrl> {
    let out = InvitationUrl("".to_string());
    Json(out)
}
