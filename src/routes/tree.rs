use crate::db;

use crate::models::censor::Censor;
use crate::models::poster::Poster;
use crate::models::node::Node;
use crate::models::tree::{self as model, Tree};
use crate::models::invitation::InvitationUrl;

use rocket::http::Status;
use rocket::response::status;
use rocket_contrib::json::Json;
use rocket_contrib::databases::rusqlite::{Connection, Result};

// lists the members of a tree
#[get("/tree/<tree_id>/list", format="json")]
pub fn list_nodes_route(conn: db::Conn, tree_id: i64) -> String {
    let nodes = list_nodes(&conn, tree_id).unwrap();
    serde_json::to_string(&nodes).expect("failed to parse vec of nodes")
}

pub fn list_nodes(conn: &Connection, tree_id: i64) -> Result<Vec<Node>> {
    model::list_tree_nodes(&conn, tree_id)
}

// lists the trees
#[get("/tree/list", format="json")]
pub fn list_trees_route(conn: db::Conn) -> String {
    let trees = list_trees(&conn).unwrap();
    serde_json::to_string(&trees).expect("failed to parse vec of nodes")
}

pub fn list_trees(conn: &Connection) -> Result<Vec<Tree>> {
    model::list_trees(&conn)
}

// creates an invite tree
#[post("/tree/create", format="json", data="<new>")]
pub fn create_route(
    conn: db::Conn,
    poster: &Poster,
    mut new: Json<Tree>
) -> status::Created<String> {
    let tree = create(&conn, &poster, &mut new)
        .expect("failed to create tree");
    let url = "/tree/create".to_string();

    status::Created(url, Some(tree.pk.to_string()))
}

pub fn create(
    conn: &Connection,
    poster: &Poster,
    new: &mut Json<Tree>,
) -> Result<Tree> {
    new.root = poster.pk;

    let count = model::count_trees(conn)?;
    
    if count > 30 {
        panic!("too many trees");
    }

    let tree = model::create_tree(conn, new)?;

    Ok(tree)
}

// bans a poster from a tree
#[post("/tree/<tree_id>/ban", format="json", data="<offender>")]
pub fn ban_route(
    conn: db::Conn,
    poster: &Poster,
    tree_id: i64,
    offender: Json<Poster>
) -> Status {
    let _res = ban(&conn, &poster, tree_id, &offender);

    Status::Ok
}

pub fn ban(
    conn: &Connection,
    poster: &Poster,
    tree_id: i64,
    offender: &Json<Poster>
) -> Result<()> {
    let censor = Censor::convert(&conn, tree_id, poster).unwrap();
    censor.ban(&conn, offender.pk)
}

// deletes a tree
#[post("/tree/<tree_id>/delete", format="json")]
pub fn delete_route(
    conn: db::Conn,
    owner: &Poster,
    tree_id: i64
) -> Status {
    let _res = delete(&conn, &owner, tree_id);

    Status::Ok
}

pub fn delete(
    conn: &Connection,
    owner: &Poster,
    tree_id: i64
) -> Result<()> {
    let censor = Censor::convert(&conn, tree_id, owner).unwrap();
    censor.delete(&conn, owner.pk)
}

// invites a poster to a tree
#[post("/tree/<invitation>", format="json", data="<invitee>")]
pub fn invite_route(
    conn: db::Conn,
    invitee: Json<Poster>,
    invitation: InvitationUrl,
) -> Status {
    invite(&conn, &invitee, &invitation)
}

pub fn invite(
    conn: &Connection,
    invitee: &Json<Poster>,
    invitation: &InvitationUrl,
) -> Status {
    invitation.valid(&conn);

    Poster {
        pk: 0,
        pubkey: invitee.pubkey.clone(),
    };

    Status::Created
}
