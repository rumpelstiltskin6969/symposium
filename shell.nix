{ pkgs ? import <nixpkgs> {} }:
  pkgs.mkShell {
    ROCKET_CLI_COLORS="off";

    buildInputs = [
      # pkgs.binutils-unwrapped
      pkgs.rustup
      pkgs.sqlite
      pkgs.python3
      pkgs.python37Packages.pyparsing
      pkgs.graphviz
      # pkgs.carnix
      # pkgs.postgresql
    ];
  }
