use crate::tests::utils;
use crate::routes::auth;

use crate::rocket;
use rocket::http::{Cookie, Cookies};
use rocket::local::Client;
use rocket::http::Status;


#[test]
fn test_login() {
    let mut conn = utils::database_conn();
    let tx = conn.transaction().expect("failed to open a transaction");

    utils::create_poster(&tx);

    auth::login(&tx, &mut Cookies::empty(), utils::HASH160);
}

#[test]
fn test_logout() {
    let mut conn = utils::database_conn();
    let tx = conn.transaction().expect("failed to open a transaction");

    utils::create_poster(&tx);

    let mut jar = Cookies::empty();
    jar.add_private(Cookie::new("poster-hash", utils::HASH160));

    auth::logout(&mut jar);
    assert_eq!(jar.get_private("poster-hash"), None);
}

#[test]
fn test_logout_route_guard() {
    let rocket = rocket();
    let client = Client::new(rocket).expect("valid rocket instance");

    let res = client
        .get("/auth/logout")
        .private_cookie(Cookie::new("poster-hash", utils::HASH160))
        .dispatch();

    // TODO(rumpelstiltskin): this test needs to be reworked
    assert_eq!(res.status(), Status::NotFound);
    assert_eq!(res.cookies().len(), 0);
}
