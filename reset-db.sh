#!/usr/bin/env sh
set -e

migrate() {
    migrations=$(ls migrations/ | grep 'up.sql' | sort)

    for m in $migrations
    do
        echo -n "$m ... "
        sqlite3 db.sqlite < "$PWD/migrations/$m"
        echo "done"
    done
}

rm -f db.sqlite

migrate

echo ''
echo 'done!'
