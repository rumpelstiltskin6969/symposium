use serde::{Serialize, Deserialize};

use crate::models::node::Node;
use rocket_contrib::databases::rusqlite::{Connection, Result};

#[derive(Debug, Serialize, Deserialize)]
pub struct Tree {
    pub pk: i64,
    pub name: String,
    pub root: i64,
}

pub fn list_trees(
    conn: &Connection,
) -> Result<Vec<Tree>> {
    let mut stmt = conn.prepare(r#"
        SELECT *
        FROM tree
    "#)?;

    let nodes = stmt.query_map(&[], |row| Tree {
        pk: row.get(0),
        name: row.get(1),
        root: row.get(2),
    })?;

    let mut res = Vec::new();

    for node in nodes {
        res.push(node?);
    }

    Ok(res)
}

pub fn list_tree_nodes(
    conn: &Connection,
    tree: i64,
) -> Result<Vec<Node>> {
    let mut stmt = conn.prepare(r#"
        SELECT *
        FROM node
        INNER JOIN tree_nodes ON node.pk = tree_nodes.node
        INNER JOIN tree ON tree.pk = tree_nodes.tree
        WHERE tree.pk = ?
    "#)?;

    let nodes = stmt.query_map(&[&tree], |row| Node {
        pk: row.get(0),
        parent: row.get(1),
        poster: row.get(2),
    })?;

    let mut res = Vec::new();

    for node in nodes {
        res.push(node?);
    }

    Ok(res)
}

pub fn find_by_pk(conn: &Connection, pk: i64) -> Result<Tree> {
    let mut stmt = conn.prepare("SELECT * FROM tree WHERE pk = ? LIMIT 1")?;

    let tree = stmt.query_row(&[&pk], |row| Tree {
        pk: row.get(0),
        name: row.get(1),
        root: row.get(2),
    })?;

    Ok(tree)
}

pub fn create_tree(conn: &Connection, tree: &Tree) -> Result<Tree> {
    let mut stmt = conn.prepare(r#"
        INSERT INTO tree (name, root)
        VALUES (?, ?)
    "#)?;

    let pk = stmt.insert(&[&tree.name, &tree.root])?;

    find_by_pk(conn, pk)
}

pub fn count_trees(conn: &Connection) -> Result<i64> {
    let mut stmt = conn.prepare(r#"SELECT COUNT(pk) FROM tree"#)?;

    let pk = stmt.query_row(&[], |row| row.get(0))?;

    Ok(pk)
}
