BEGIN;

CREATE TABLE IF NOT EXISTS poster (
    pk INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
    pubkey TEXT UNIQUE NOT NULL
);

CREATE TABLE IF NOT EXISTS tree (
    pk INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
    name TEXT UNIQUE NOT NULL,
    root INTEGER NOT NULL,

    FOREIGN KEY (root) REFERENCES node (pk)
);

CREATE TABLE IF NOT EXISTS node (
    pk INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
    parent INTEGER,
    self INTEGER NOT NULL,

    FOREIGN KEY (parent) REFERENCES poster (pk)
    FOREIGN KEY (self) REFERENCES poster (pk)
);

CREATE TABLE IF NOT EXISTS tree_nodes (
    tree INTEGER NOT NULL,
    node INTEGER NOT NULL,

    FOREIGN KEY (tree) REFERENCES tree (pk)
    FOREIGN KEY (node) REFERENCES node (pk)

    PRIMARY KEY (tree, node)
);

CREATE TABLE IF NOT EXISTS tree_options (
    tree INTEGER NOT NULL,
    opt INTEGER NOT NULL,

    FOREIGN KEY (tree) REFERENCES tree (pk)

    PRIMARY KEY (tree, opt)
);

CREATE TABLE IF NOT EXISTS node_options (
    node INTEGER NOT NULL,
    role INTEGER NOT NULL,
    opt INTEGER NOT NULL,

    FOREIGN KEY (node) REFERENCES node (pk)

    PRIMARY KEY (node, opt)
);

COMMIT;
