use crate::models::tree::{Tree};

use rocket_contrib::json::Json;

use crate::tests::utils;
use crate::routes::tree as routes;

#[test]
fn test_list_trees() {
    let mut conn = utils::database_conn();
    let tx = conn.transaction().expect("failed to open a transaction");

    let initial_accounts = utils::list_posters(&tx);
    assert_eq!(initial_accounts.len(), 0);

    utils::create_poster(&tx);
    let trees = routes::list_trees(&tx).unwrap();

    assert_eq!(trees.len(), 0);
}

#[test]
fn test_create_tree() {
    let mut conn = utils::database_conn();
    let tx = conn.transaction().expect("failed to open a transaction");

    let initial_accounts = utils::list_posters(&tx);
    assert_eq!(initial_accounts.len(), 0);

    let poster = utils::create_poster(&tx);

    let tree = Tree {
        pk: 0, 
        name: "pol".to_string(),
        root: 1,
    };

    let mut data = Json(tree);
    routes::create(&tx, &poster, &mut data).unwrap();

    let trees = routes::list_trees(&tx).unwrap();
    assert_eq!(trees.len(), 1);
}

#[test]
fn test_tree_invite() {
    let mut conn = utils::database_conn();
    let tx = conn.transaction().expect("failed to open a transaction");

    let initial_accounts = utils::list_posters(&tx);
    assert_eq!(initial_accounts.len(), 0);

    let poster = utils::create_poster(&tx);

    let tree = Tree {
        pk: 0, 
        name: "pol".to_string(),
        root: 1,
    };

    let mut data = Json(tree);
    routes::create(&tx, &poster, &mut data).unwrap();

    let trees = routes::list_trees(&tx).unwrap();
    assert_eq!(trees.len(), 1);
}

//#[test]
//fn test_delete_tree() {
//    let mut conn = utils::database_conn();
//    let tx = conn.transaction().expect("failed to open a transaction");
//
//    let initial_accounts = utils::list_posters(&tx);
//    assert_eq!(initial_accounts.len(), 0);
//
//    let poster = utils::create_poster(&tx);
//
//    let tree = Tree {
//        pk: 0,
//        name: "fit".to_string(),
//        root: 1,
//    };
//
//    let mut data = Json(tree);
//    routes::create(&tx, &poster, &mut data).unwrap();
//
//    let trees = routes::list_trees(&tx).unwrap();
//    assert_eq!(trees.len(), 1);
//
//    routes::delete(&tx, &poster, 0).unwrap();
//}
//
//#[test]
//fn test_tree_ban_user() {
//    let rocket = rocket();
//    let client = Client::new(rocket).expect("valid rocket instance");
//
//    let mut response = client
//        .get("/tree/0/list")
//        .dispatch();
//
//    assert_eq!(response.status(), Status::Ok);
//    assert_eq!(response.body_string(), Some("[]".to_string()));
//}
