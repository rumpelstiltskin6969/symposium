use crate::models::node::{NodeOpt, Permission};
use crate::models::poster::Poster;
use rocket_contrib::databases::rusqlite::{Connection, Result};

pub struct Censor(Poster);

impl Censor {
    pub fn ban(&self, conn: &Connection, offender: i64) -> Result<()> {
        let mut stmt = match conn.prepare("
            INSERT INTO node_options (node, opt)
            VALUES (?, ?)
        ") {
            Ok(v) => v,
            Err(e) => panic!("could not prepare ban statement for censor {:?}", e),
        };

        let opt = NodeOpt::BANNED as i64;
        let qres = stmt.query_row(&[&offender, &opt], |row| Poster {
            pk: row.get(0),
            pubkey: row.get(1),
        });

        let _ = match qres {
            Ok(v) => v,
            Err(e) => panic!("err when querying ban censor: {:?}", e),
        };

        Ok(())
    }

    pub fn delete(&self, conn: &Connection, offender: i64) -> Result<()> {
        let mut stmt = match conn.prepare("
            INSERT INTO node_options (node, opt)
            VALUES (?, ?)
        ") {
            Ok(v) => v,
            Err(e) => panic!("could not prepare delete statement for censor {:?}", e),
        };

        let opt = NodeOpt::DELETED as i64;
        let qres = stmt.query_row(&[&offender, &opt], |row| Poster {
            pk: row.get(0),
            pubkey: row.get(1),
        });

        let _ = match qres {
            Ok(v) => v,
            Err(e) => panic!("err when querying del censor: {:?}", e),
        };

        Ok(())
    }

    pub fn convert(conn: &Connection, tree_id: i64, poster: &Poster) -> Option<Censor> {
        let mut stmt = match conn.prepare("
            SELECT opt
            FROM node_options
            INNER JOIN node ON node.pk = node_options.node
            INNER JOIN poster ON poster.pk = node_options.node
            INNER JOIN tree_nodes ON tree_nodes.node = node.pk
            WHERE poster.pubkey = ? 
            AND node_options.opt in (?, ?)
            AND tree_nodes.tree = ?
            LIMIT 1
        ") {
            Ok(v) => v,
            Err(e) => panic!("could not prepare statement for censor {:?}", e),
        };

        let opt_ban = Permission::Ban as i64;
        let opt_del = Permission::Delete as i64;
        let qres = stmt.query_row(
            &[&poster.pubkey, &opt_ban, &opt_del, &tree_id],
            |row| Poster {
                pk: row.get(0),
                pubkey: row.get(1),
            }
        );

        let poster = match qres {
            Ok(v) => v,
            Err(e) => panic!("err when querying censor: {:?}", e),
        };

        Some(Censor {
            0: poster,
        })
    }
}
