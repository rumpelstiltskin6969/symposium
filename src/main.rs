#![feature(never_type)]
#![feature(proc_macro_hygiene, decl_macro)]

extern crate bip39;
extern crate bitcoin;
extern crate bitcoin_hashes;
extern crate id_tree;
#[macro_use] extern crate rocket;
#[macro_use] extern crate rocket_contrib;

mod db;
mod models;
mod routes;
#[cfg(test)] mod tests;

fn rocket() -> rocket::Rocket {
    rocket::ignite()
        .attach(db::Conn::fairing())
        .mount("/", routes![
            routes::index::index_route,
            routes::poster::create_route,
            routes::poster::new_mnemonic_route,
            routes::tree::invite_route,
            routes::tree::ban_route,
            routes::tree::delete_route,
            routes::tree::list_nodes_route,
            routes::tree::list_trees_route,
            routes::tree::create_route,
            routes::auth::login_route,
            routes::auth::logout_route,
        ])
}

fn main() {
    rocket().launch();
}
