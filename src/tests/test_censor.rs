use crate::tests::utils;

#[test]
fn test_create_censor() {
    let mut conn = utils::database_conn();
    let tx = conn.transaction().expect("failed to open a transaction");

    let initial_accounts = utils::list_posters(&tx);
    assert_eq!(initial_accounts.len(), 0);

    let poster = utils::create_poster(&tx);

    let created_accounts = utils::list_posters(&tx);

    assert_eq!(created_accounts.len(), 1);
    assert_eq!(created_accounts[0].pk, 1);
    assert_eq!(created_accounts[0].pubkey, utils::HASH160);

    assert_eq!(poster.pk, created_accounts[0].pk);
    assert_eq!(poster.pubkey, utils::HASH160);
}
