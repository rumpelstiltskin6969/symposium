use serde::{Serialize, Deserialize};

use crate::models::poster::Poster;

use rocket::http::RawStr;
use rocket::request::FromParam;
use rocket_contrib::databases::rusqlite::Connection;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct InvitationUrl(pub String);

impl InvitationUrl {
    pub fn invite(&self, db: &Connection) -> Result<(), !> {
        Ok(())
    }

    pub fn valid(&self, db: &Connection) -> Result<(), !> {
        Ok(())
    }
}

impl<'r> FromParam<'r> for InvitationUrl {
    type Error = &'r RawStr;

    fn from_param(param: &'r RawStr) -> Result<Self, Self::Error> {
        Ok(InvitationUrl(
            param.to_string()
        ))
    }
}

pub fn create(db: &Connection, poster: &Poster) -> InvitationUrl {
    InvitationUrl("".to_string())
}
