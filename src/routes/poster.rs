use crate::db;
use crate::models::poster::{self as model, Poster};

use rocket_contrib::databases::rusqlite::Connection;
use rocket_contrib::json::Json;

use bip39::{Mnemonic, MnemonicType, Language, Seed};
use bitcoin::network::constants::Network;
use bitcoin::util::bip32::{ExtendedPrivKey, ExtendedPubKey};
use bitcoin::secp256k1::{Secp256k1, Signing};

struct KeyPair {
    pub private: ExtendedPrivKey,
    pub public: ExtendedPubKey,
}

#[post("/poster/create", format="json", data="<mnemonic>")]
pub fn create_route(conn: db::Conn, mnemonic: String) -> Json<Poster> {
    let res = create(&conn, &mnemonic.to_string());

    Json(res)
}

pub fn create(
    conn: &Connection,
    mnemonic: &String,
) -> Poster {
    Mnemonic::validate(&mnemonic, Language::English).unwrap();

    let mnemonic = &Mnemonic::from_phrase(mnemonic, Language::English).unwrap();
    let seed = Seed::new(mnemonic, "");

    let secp = Secp256k1::new();
    let kp = generate_keypair(&secp, &seed.as_bytes());
    let hash160 = kp.public.identifier();

    model::create(conn, hash160).unwrap()
}

fn generate_keypair<C: Signing>(secp: &Secp256k1<C>, seed: &[u8]) -> KeyPair {
    let priv_key = ExtendedPrivKey::new_master(Network::Regtest, &seed).expect("failed to create private key");
    let pub_key = ExtendedPubKey::from_private(&secp, &priv_key);

    KeyPair {
        private: priv_key,
        public: pub_key,
    }
}

#[get("/poster/mnemonic", format="plain")]
pub fn new_mnemonic_route() -> String {
    let mnemonic = Mnemonic::new(MnemonicType::Words12, Language::English);
    mnemonic.phrase().to_string()
}
