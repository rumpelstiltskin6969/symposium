use serde::{Serialize, Deserialize};

use crate::db;
use rocket::outcome::IntoOutcome;
use rocket::request::{self, Request, FromRequest};
use rocket_contrib::databases::rusqlite::{Connection, Result};

use bitcoin_hashes::hash160::Hash;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Poster {
    pub pk: i64,
    pub pubkey: String,
}

impl<'a, 'r> FromRequest<'a, 'r> for &'a Poster {
    type Error = !;

    fn from_request(request: &'a Request<'r>) -> request::Outcome<Self, Self::Error> {
        let poster = request.local_cache(|| {
            let db = request.guard::<db::Conn>().succeeded()?;

            request.cookies()
                .get_private("poster-hash")
                .and_then(|cookie| Some(cookie.value().to_string()))
                .and_then(|hash| find_by_pubkey(&db, &hash).ok())
        });

        poster.as_ref().or_forward(())
    }
}

pub fn find_by_pubkey(conn: &Connection, pubkey: &str) -> Result<Poster> {
    let mut stmt = conn.prepare("SELECT * FROM poster WHERE pubkey = ? LIMIT 1")?;

    let poster = stmt.query_row(&[&pubkey], |row| Poster {
        pk: row.get(0),
        pubkey: row.get(1),
    })?;

    Ok(poster)
}

pub fn find_by_pk(conn: &Connection, pk: i64) -> Result<Poster> {
    let mut stmt = conn.prepare("SELECT * FROM poster WHERE pk = ? LIMIT 1")?;

    let poster = stmt.query_row(&[&pk], |row| Poster {
        pk: row.get(0),
        pubkey: row.get(1),
    })?;

    Ok(poster)
}

pub fn create(
    conn: &Connection,
    hash160: Hash,
) -> Result<Poster> {
    let hash = hash160.to_string();
    let mut stmt = conn.prepare("INSERT INTO poster (pubkey) VALUES (?)")?;

    let pk = stmt.insert(&[&hash])?;

    find_by_pk(conn, pk)
}

//pub fn get_permissions(
//    conn: &Connection,
//    poster: i64
//) -> Result<Vec<(i64, Vec<Permission>)>> {
//    let mut stmt = conn.prepare(r#"
//        SELECT tree.pk, node.permissions
//        FROM node
//        INNER JOIN tree_nodes ON node.pk = tree_nodes.node
//        INNER JOIN tree ON tree.pk = tree_nodes.tree
//        INNER JOIN poster ON node.pk = poster.pk
//        WHERE node.pk = ?
//    "#)?;
//
//    let _rows = stmt.query(&[&poster])?;
//
//    Ok(Vec::new())
//}
