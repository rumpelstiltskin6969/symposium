// always use a read limit to prevent DoS attacks
const READ_LIMIT: u64 = 256;
