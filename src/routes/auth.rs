use crate::db;
use crate::models::poster;

use rocket_contrib::databases::rusqlite::Connection;
use rocket::http::{Cookies, Cookie};

// sets a secret cookie for the poster
#[post("/auth/login", format="json", data="<hash160>")]
pub fn login_route(conn: db::Conn, mut cookies: Cookies, hash160: String) -> () {
    login(&conn, &mut cookies, &hash160)
}

pub fn login(
    conn: &Connection,
    cookies: &mut Cookies,
    hash160: &str
) -> () {
    let poster = poster::find_by_pubkey(&conn, &hash160).unwrap();
    cookies.add_private(Cookie::new("poster-hash", poster.pubkey));
}

// deletes the login cookie
#[get("/auth/logout")]
pub fn logout_route(_poster: &poster::Poster, mut cookies: Cookies) -> () {
    logout(&mut cookies)
}

pub fn logout(
    cookies: &mut Cookies,
) -> () {
    cookies.remove_private(Cookie::named("poster-hash"));
}
