use serde::{Serialize, Deserialize};

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Node {
    pub pk: i64,
    pub parent: Option<i64>,
    pub poster: i64,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum Permission {
    Ban = 1,
    Invite = 2,
    Delete = 3,
    Post = 4,
    Read = 5,
    FuckOff = 6,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum Role {
    God = 1,
    GlobalCensor = 2,
    Owner = 3,
    Censor = 4,
    Poster = 5,
    Slave = 6,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum NodeOpt {
    BANNED = 1,
    DELETED = 2,
}
