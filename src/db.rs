use rocket_contrib::databases::rusqlite::Connection;

#[database("dev_database")]
pub struct Conn(Connection);
