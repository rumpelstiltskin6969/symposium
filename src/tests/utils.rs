use crate::db;
use crate::routes::poster as routes;
use crate::models::poster::Poster;

use rocket_contrib::databases::rusqlite::Connection;
use rocket_contrib::databases::r2d2::Pool;
use rocket_contrib::databases::r2d2_sqlite::SqliteConnectionManager;

pub const MNEMONIC: &str = "risk energy oblige rescue song upper physical depend grant width sunset interest";
pub const HASH160: &str = "42c1ec9d7ccbe4456f88230bedc61483b628e1ba";

pub fn database_conn() -> db::Conn {
    let manager = SqliteConnectionManager::file("db.sqlite");
    let pool = Pool::builder()
        .max_size(1)
        .build(manager)
        .unwrap();

    db::Conn(pool.get().unwrap())
}

pub fn create_poster(conn: &Connection) -> Poster {
    routes::create(&conn, &MNEMONIC.to_string())
}

pub fn list_posters(conn: &Connection) -> Vec<Poster> {
    let mut stmt = conn.prepare("SELECT * FROM poster").unwrap();

    let posters = stmt.query_map(&[], |row| Poster {
        pk: row.get(0),
        pubkey: row.get(1),
    }).unwrap();

    let mut res = Vec::new();

    for poster in posters {
        res.push(poster.unwrap());
    }

    res
}
